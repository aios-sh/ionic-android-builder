FROM docker.io/ubuntu:20.04

ARG JDK_VERSION="13"
ENV JDK_VERSION "${JDK_VERSION}"

ENV TZ "UTC"
RUN ln -snf /usr/share/zoneinfo/UTC /etc/localtime && echo UTC > /etc/timezone

RUN apt update && apt install -y openjdk-${JDK_VERSION}-jdk curl wget unzip zipalign git git-crypt && \
    curl -fsSL https://deb.nodesource.com/setup_16.x | bash - && \
    apt install -y nodejs && apt clean all

ENV ANDROID_SDK_ROOT "/opt/cmdline-tools"
ENV CI "true"

RUN wget https://dl.google.com/android/repository/commandlinetools-linux-7583922_latest.zip -O /tmp/sdk.zip && \
    unzip /tmp/sdk.zip -d /opt && rm -f /tmp/sdk.zip

RUN npm install -g @ionic/cli serve --cache /tmp/npm && rm -rf /tmp/npm

ENV JAVA_HOME "/usr/lib/jvm/java-${JDK_VERSION}-openjdk-amd64"
