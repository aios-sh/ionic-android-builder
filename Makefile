REGISTRY ?= registry.gitlab.com/aios-sh/ionic-android-builder
VERSION ?= $(shell git log -n 1 '--pretty=format:%h')
JDK_VERSION ?= 13
BRANCH ?= $(shell git branch --show-current)
IMAGE ?= $(REGISTRY):jdk-$(JDK_VERSION)-$(VERSION)
IMAGE_RELEASE ?= $(REGISTRY):jdk-$(JDK_VERSION)-$(BRANCH)

all: build push

build:
	docker build -t $(IMAGE) --build-arg JDK_VERSION=$(JDK_VERSION) .

release:
	docker tag $(IMAGE) $(IMAGE_RELEASE)
	docker push $(IMAGE_RELEASE)

update-latest:
	BRANCH=latest make release

push:
	docker push $(IMAGE)
